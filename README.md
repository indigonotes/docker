# Docker
## Dockerfile
Used to build containers
.dockerignore usefull to improve build's performance
Dockerfile usually located in root of context, can specify path:
```bash
docker build -f /path/to/a/Dockerfile .
```
Validated by docker daemon. returns error if syntax is incorrect
### Instructions 
```
INSTRUCTION arguments
```
INSTRUCTION name convention in caps
INTRUCTIONS ran in order
dockerfile must begin with FROM instruction
#### FROM
FROM may be after comments, parser directives or ARGs
Specifies parent image
#### Comment
```
# comment
```
Lines that begin with # are treated as cmments
#### Parser directives
Optional
Affect sub